//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MVCBase
{
    using System;
    using System.Collections.Generic;
    
    public partial class Files
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Files()
        {
            this.Fields = new HashSet<Fields>();
        }
    
        public int IdFile { get; set; }
        public string NameFile { get; set; }
        public Nullable<int> NumFila { get; set; }
        public Nullable<int> NumColumns { get; set; }
        public Nullable<int> IdDbInstance { get; set; }
        public string TableName { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Fields> Fields { get; set; }
        public virtual Instances Instances { get; set; }
    }
}
