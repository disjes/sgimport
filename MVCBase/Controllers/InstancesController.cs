﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCBase;

namespace MVCBase.Controllers
{
    public class InstancesController : Controller
    {
        private SGImportEntities db = new SGImportEntities();

        // GET: Instances
        public ActionResult Index()
        {
            return View(db.Instances.ToList());
        }

        // GET: Instances/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Instances instances = db.Instances.Find(id);
            if (instances == null)
            {
                return HttpNotFound();
            }
            return View(instances);
        }

        // GET: Instances/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Instances/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdInstance,NameInstance,ConnectionString")] Instances instances)
        {
            if (ModelState.IsValid)
            {
                db.Instances.Add(instances);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(instances);
        }

        // GET: Instances/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Instances instances = db.Instances.Find(id);
            if (instances == null)
            {
                return HttpNotFound();
            }
            return View(instances);
        }

        // POST: Instances/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdInstance,NameInstance,ConnectionString")] Instances instances)
        {
            if (ModelState.IsValid)
            {
                db.Entry(instances).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(instances);
        }

        // GET: Instances/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Instances instances = db.Instances.Find(id);
            if (instances == null)
            {
                return HttpNotFound();
            }
            return View(instances);
        }

        // POST: Instances/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Instances instances = db.Instances.Find(id);
            db.Instances.Remove(instances);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
