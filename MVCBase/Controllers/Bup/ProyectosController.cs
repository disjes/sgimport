﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.WebSockets;
using CrystalDecisions.Shared;
using MVCBase;
using MVCBase.Models;

namespace MVCBase.Controllers
{
    public class ProyectosController : Controller
    {
        private MODERNIZACION_DBEntities db = new MODERNIZACION_DBEntities();

        // GET: Proyectos
        public ActionResult Index()
        {
            return View(db.cxmProyectos.ToList());
        }

        // GET: Proyectos/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cxmProyectos cxmProyectos = db.cxmProyectos.Find(id);
            if (cxmProyectos == null)
            {
                return HttpNotFound();
            }
            return View(cxmProyectos);
        }

        // GET: Proyectos/Details/5
        public ActionResult Preview(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cxmProyectos proy = db.cxmProyectos.Find(id);                  
            if (proy == null)
            {
                return HttpNotFound();
            }
            List<CuentasPorFranjaViewModel> cuentas = new List<CuentasPorFranjaViewModel>();
            foreach (var cxmFranja in proy.cxmFranja)
            {                                                                   
                var factorConversion = (proy.montoEjecutado * cxmFranja.porcentaje / 100) / GetAreaTotal(proy);
                foreach (var inmueble in cxmFranja.cxmInmueblesPorProyecto)
                    cuentas.Add(PerformCalculos(CreateCuenta(inmueble), proy, factorConversion));
            }
            ViewBag.cuentas = cuentas;
            ViewBag.idFactorSocioEconomico = new SelectList(db.FactorSocioEconomico, "idFactorSocioEconomico", "factorSocioEconomico1");
            ViewBag.idFactorUso = new SelectList(db.FactorUso, "idFactorUso", "factorUso1");
            ViewBag.idFactorArea = new SelectList(db.FactorArea, "idFactorArea", "value");
            ViewBag.idAccesoAjuste = new SelectList(db.AccesoAjuste, "idAccesoAjuste", "value");
            ViewBag.idFactorDistancia = new SelectList(db.FactorDistancia, "idFactorDistancia", "value");           
            ViewBag.idFranja = new SelectList(db.cxmFranja.Where(x => x.idProyecto == proy.idProyecto), "idFranja", "numFranja");
            ViewBag.franjas = ViewBag.idFranja;
            ViewBag.factores = new SelectList(new [] { "Factor Área", "Factor Socioeconómico", "Acceso Ajuste", "Factor Distancia", "Factor Uso" }.Select((f, index) => 
                new { Text = f, Value = index.ToString() }), "Value", "Text");
            ViewBag.valores = new SelectList(db.FactorArea, "idFactorArea", "value");
            ViewBag.errorMessage = TempData["errorMessage"];
            ViewBag.added = TempData["added"];
            this.ControllerContext.HttpContext.Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return View(proy);
        }

        public ActionResult ValoresPerFactor(int factor)
        {
            if (factor == 0) ViewBag.valores = new SelectList(db.FactorArea, "idFactorArea", "value");
            if (factor == 1) ViewBag.valores = new SelectList(db.FactorSocioEconomico, "idFactorSocioEconomico", "value");
            if (factor == 2) ViewBag.valores = new SelectList(db.AccesoAjuste, "idAccesoAjuste", "value");
            if (factor == 3) ViewBag.valores = new SelectList(db.FactorDistancia, "idFactorDistancia", "value");
            if (factor == 4) ViewBag.valores = new SelectList(db.FactorUso, "idFactorUso", "value");
            return PartialView();
        }
        
        public ActionResult InsertCatCca(cxmInmueblesPorProyecto inmueblexProyecto, int idProyecto)//int idProyecto, int franja, string cuenta, int factorArea, int factorSocioEconomico, int accesoAjuste, int factorUso, int factorDistancia)
        {
            var inmueble = db.consultaInmuebles.FirstOrDefault(x => x.CatCca == inmueblexProyecto.catCca.Replace("-", ""));
            if (inmueble != null)
            {                
                inmueblexProyecto.fecha = DateTime.Now;
                var existe = 
                    db.cxmInmueblesPorProyecto.FirstOrDefault(
                        x => x.cxmFranja.idProyecto == idProyecto && x.catCca == inmueblexProyecto.catCca);
                if (existe == null)
                {
                    db.cxmInmueblesPorProyecto.Add(inmueblexProyecto);
                    db.SaveChanges();
                    TempData["added"] = "YES";
                } else TempData["errorMessage"] = "El inmueble ya fue registrado en este proyecto.";
            } else TempData["errorMessage"] = "El inmueble fue escrito incorrectamente o no existe en la base de datos.";
            return RedirectToAction("Preview", new { id = idProyecto });
        }

        public ActionResult UpdateFranja(int franjas, int factores, int valores, int idProyecto)
        {
            var franja = db.cxmFranja.FirstOrDefault(x => x.idFranja == franjas);
            foreach (var inmueble in franja.cxmInmueblesPorProyecto)
            {
                if (factores == 0) inmueble.idFactorArea = valores;
                if (factores == 1) inmueble.idFactorSocioEconomico = valores;
                if (factores == 2) inmueble.idAccesoAjuste = valores;
                if (factores == 3) inmueble.idFactorDistancia = valores;
                if (factores == 4) inmueble.idFactorUso = valores;
                db.Entry(inmueble).State = EntityState.Modified;
            }
            db.SaveChanges();
            return RedirectToAction("Preview", new { id = idProyecto });
        }

        private decimal GetAreaFranja(int idFranja)
        {
            var franja = db.cxmFranja.FirstOrDefault(x => x.idFranja == idFranja);
            List<consultaInmuebles> cInmueblesList = new List<consultaInmuebles>();
            if (franja != null)
            {
                var inmuebles = franja.cxmInmueblesPorProyecto;
                decimal areasReales = 0;                
                foreach (var inmueble in inmuebles)
                {
                    var consultaInmueble = db.consultaInmuebles.FirstOrDefault(x => x.CatCca == inmueble.catCca.Replace("-", "").Replace(" ", ""));
                    cInmueblesList.Add(consultaInmueble);
                    if (consultaInmueble?.CatAre!= null) areasReales += (decimal)consultaInmueble.CatAre;
                    if (consultaInmueble == null || consultaInmueble.CatAre == null || consultaInmueble.CatAre == 0)
                    {
                        int a = 0;
                    }
                }
                ViewBag.cInmueblesList = cInmueblesList;
                return areasReales;
            }            
            return 0;
        }

        private decimal GetAreaTotal(cxmProyectos proy)
        {
            //List<consultaInmuebles> cInmueblesList = new List<consultaInmuebles>();
            if (proy != null)
            {
                var inmuebles = new List<cxmInmueblesPorProyecto>();
                foreach (var franja in proy.cxmFranja) inmuebles.AddRange(franja.cxmInmueblesPorProyecto);           
                decimal areasReales = 0;
                foreach (var inmueble in inmuebles)
                {
                    //var consultaInmueble = db.consultaInmuebles.FirstOrDefault(x => x.CatCca == inmueble.catCca.Replace("-", "").Replace(" ", ""));
                    //cInmueblesList.Add(consultaInmueble);
                    areasReales += ((decimal)inmueble.area == null) ? 0 : (decimal)inmueble.area;
                    if (inmueble == null || inmueble.area == null || inmueble.area == 0)
                    {
                        int a = 0;
                    }
                }
                //ViewBag.cInmueblesList = cInmueblesList;
                return areasReales;
            }
            return 0;
        }

        private CuentasPorFranjaViewModel CreateCuenta(cxmInmueblesPorProyecto inmueble)
        {
            var cInmueblesList = ViewBag.cInmueblesList as List<consultaInmuebles>;
            //var area = cInmueblesList.FirstOrDefault(x => x.CatCca.Replace(" ", "") == inmueble.catCca.Replace("-", "").Replace(" ", ""))?.CatAre;
            //var beforeViewModel = SetErrorsBeforeViewModelCreation(inmueble, (decimal)area);
            //if (beforeViewModel == null)
                return new CuentasPorFranjaViewModel()
            {
                IdInmueblePorProyecto = inmueble.idInmueblePorProyecto,
                NumFranja = inmueble.cxmFranja.numFranja,
                CatCca = inmueble.catCca,
                AreaTerreno = inmueble.area,
                FactorDistancia = ((inmueble.idFactorDistancia == null) ? null : inmueble.FactorDistancia.value),
                FactorArea = ((inmueble.idFactorArea == null) ? null : inmueble.FactorArea.value),
                FactorUso = ((inmueble.idFactorUso == null) ? null : inmueble.FactorUso?.value),
                SocioEconomico = ((inmueble.idFactorSocioEconomico == null) ? null : inmueble.FactorSocioEconomico.value),
                AccesoAjuste = ((inmueble.idAccesoAjuste == null) ? null : inmueble.AccesoAjuste.value)
            };
            //return beforeViewModel;
        }

        private CuentasPorFranjaViewModel SetErrorsBeforeViewModelCreation(cxmInmueblesPorProyecto inmueble, decimal area)
        {
            if (inmueble.idFactorDistancia == null || inmueble.idFactorArea == null ||inmueble.idFactorUso == null ||
                inmueble.idFactorSocioEconomico == null || inmueble.idAccesoAjuste == null)
                    return new CuentasPorFranjaViewModel()
                    {
                        IdInmueblePorProyecto = inmueble.idInmueblePorProyecto,
                        NumFranja = inmueble.cxmFranja.numFranja,
                        CatCca = inmueble.catCca,
                        AreaTerreno = area,
                        FactorDistancia = 0,
                        FactorArea = 0,
                        FactorUso = 0,
                        SocioEconomico = 0,
                        AccesoAjuste = 0
                    };            
            return null;
        }

        private CuentasPorFranjaViewModel PerformCalculos(CuentasPorFranjaViewModel cuentasPorFranja, cxmProyectos proyecto, decimal? factorConversion)
        {
            string validations = validateFields(cuentasPorFranja, factorConversion);
            if (validations == "Ok")
            {
                cuentasPorFranja.FactorIndividual = Convert.ToDecimal((
                    cuentasPorFranja.FactorDistancia*
                    cuentasPorFranja.FactorArea*
                    cuentasPorFranja.FactorUso*
                    cuentasPorFranja.SocioEconomico*
                    cuentasPorFranja.AccesoAjuste
                ));
                cuentasPorFranja.AreaGravable = (decimal)cuentasPorFranja.FactorIndividual*(decimal) cuentasPorFranja.AreaTerreno;
                cuentasPorFranja.FactorConversion = (double) factorConversion;
                cuentasPorFranja.Gravamen = Convert.ToDouble(cuentasPorFranja.AreaGravable) * cuentasPorFranja.FactorConversion;
                setValuesToStrs(cuentasPorFranja);
            } else setErrorToFields(cuentasPorFranja, validations);
            return cuentasPorFranja;            
        }

        private void setValuesToStrs(CuentasPorFranjaViewModel cuentasPorFranja)
        {
            cuentasPorFranja.FactorIndividualStr = cuentasPorFranja.FactorIndividual.ToString();
            cuentasPorFranja.AreaGravableStr = cuentasPorFranja.AreaGravable.ToString();
            cuentasPorFranja.FactorConversionStr = cuentasPorFranja.FactorConversion.ToString();
            cuentasPorFranja.GravamenStr = cuentasPorFranja.Gravamen.ToString();
        }

        private void setErrorToFields(CuentasPorFranjaViewModel cuentasPorFranja, string message)
        {
            cuentasPorFranja.FactorIndividualStr = message;
            cuentasPorFranja.AreaGravableStr = message;
            cuentasPorFranja.FactorConversionStr = message;
            cuentasPorFranja.GravamenStr = message;
        }

        private string validateFields(CuentasPorFranjaViewModel cuentasPorFranja, decimal? factorConversion)
        {
            if (cuentasPorFranja.FactorArea == null) return "Revisar Factor Área";
            if (cuentasPorFranja.FactorUso == null) return "Revisar Factor Uso";
            if (cuentasPorFranja.SocioEconomico == null) return "Revisar Factor Socioeconomico";
            if (cuentasPorFranja.AccesoAjuste == null) return "Revisar Acceso Ajuste";
            if (factorConversion == 0) return "Revisar Las Áreas de Terrenos en Franja";
            if (cuentasPorFranja.AreaTerreno == null)
                return "Revisar Área Terreno";
            if (cuentasPorFranja.FactorDistancia == null || Math.Abs((double)cuentasPorFranja.FactorDistancia) < 0.0)
                return "Revisar Factor Distancia";
            if (cuentasPorFranja.FactorArea == null) return "Revisar Factor Área";
            if (cuentasPorFranja.FactorUso == null || Math.Abs((double)cuentasPorFranja.FactorUso) < 0.0)
                return "Revisar Factor Uso";
            if (cuentasPorFranja.SocioEconomico == null) return "Revisar Factor SocioEconómico";
            if (cuentasPorFranja.AccesoAjuste == null) return "Revisar Acceso Ajuste";
            return "Ok";
        }   

        public string getLastTicket()
        {
            return "'<p>CCCCC0587</p>'";
        }

        // GET: Proyectos/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Proyectos/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idProyecto,cxmFranja,cxmInmueblesPorProyecto,nombreProyecto,ubicacion,descripcion,contratista,valorProyecto,montoContratado,montoEjecutado,areaContratada,areaEjecutada,medidaArea,fechaInicio,fechaFinalizacion,cantidadFranjas")] cxmProyectos cxmProyectos)
        {
            if (ModelState.IsValid)
            {
                SetDefaultValuestToInmuebles(cxmProyectos);
                db.cxmProyectos.Add(cxmProyectos);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cxmProyectos);
        }

        private void SetDefaultValuestToInmuebles(cxmProyectos cxmProyectos)
        {
            foreach (var franja in cxmProyectos.cxmFranja)
            {
                foreach (var inmueble in franja.cxmInmueblesPorProyecto) SetDefaultValuestToInmueble(inmueble);                
            }
        }

        private void SetDefaultValuestToInmueble(cxmInmueblesPorProyecto inmueble)
        {
            inmueble.idFactorArea = db.FactorArea.OrderBy(x => x.idFactorArea).First().idFactorArea;
            inmueble.idFactorSocioEconomico = db.FactorSocioEconomico.OrderBy(x => x.idFactorSocioEconomico).First().idFactorSocioEconomico;
            inmueble.idAccesoAjuste = db.AccesoAjuste.OrderBy(x => x.idAccesoAjuste).First().idAccesoAjuste;
            inmueble.idFactorDistancia = db.FactorDistancia.OrderBy(x => x.idFactorDistancia).First().idFactorDistancia;
            var consultaInmueble = db.consultaInmuebles.FirstOrDefault(x => x.CatCca == inmueble.catCca);
            if (consultaInmueble != null) inmueble.area = consultaInmueble.CatAre;
        }

        // GET: Proyectos/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cxmProyectos cxmProyectos = db.cxmProyectos.Find(id);
            if (cxmProyectos == null)
            {
                return HttpNotFound();
            }
            return View(cxmProyectos);
        }

        // POST: Proyectos/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idProyecto,nombreProyecto,ubicacion,descripcion,contratista,valorProyecto,montoContratado,montoEjecutado,areaContratada,areaEjecutada,medidaArea,fechaInicio,fechaFinalizacion,cantidadFranjas")] cxmProyectos cxmProyectos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cxmProyectos).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cxmProyectos);
        }        

        [HttpPost]
        public ActionResult PerformSubmit(string addCca, string closeCarga, string printDocs, string createCc, string idProyecto)
        {   
            if (addCca != null) return RedirectToAction("Preview");
            if (closeCarga != null) return Index();
            if (printDocs != null) return Index();
            if (createCc != null) return CrearCuentasCorrientes(Convert.ToInt32(idProyecto));
            return RedirectToAction("Index");
        }

        // GET: Proyectos/Cuenta/5
        public ActionResult CrearCuentasCorrientes(int idProyecto)
        {
            var proyecto = db.cxmProyectos.FirstOrDefault(x => x.idProyecto == idProyecto);
            var tipoCuenta = 1;
            foreach (var franja in proyecto.cxmFranja)
            {                
                foreach (var inmueble in franja.cxmInmueblesPorProyecto)
                {
                    var ccPorProyecto = new CuentasCorrientesPorProyecto()
                    {
                        idCuentaCorriente = 0,                        
                        idProyecto = idProyecto,
                        CuentasCorrientes = new CuentasCorrientes()
                        {
                            idCuenta = 0,
                            catCca = inmueble.catCca,
                            tipoCuenta = tipoCuenta,
                            montoAdeudado = inmueble.area * (inmueble.FactorDistancia.value * 
                                                                inmueble.FactorArea.value *
                                                                inmueble.FactorUso.value * 
                                                                inmueble.FactorSocioEconomico.value * 
                                                                inmueble.AccesoAjuste.value) * 0,
                            fecha = DateTime.Now
                            // cuentasPorFranja.FactorIndividual * (decimal)cuentasPorFranja.AreaTerreno                
                        }
                    };
                    db.CuentasCorrientesPorProyecto.Add(ccPorProyecto);
                }                                                                                
            }
            db.SaveChanges();
            return RedirectToAction("Index", "CuentasCorrientes");
        }

        // GET: Proyectos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            cxmProyectos cxmProyectos = db.cxmProyectos.Find(id);
            if (cxmProyectos == null)
            {
                return HttpNotFound();
            }
            return View(cxmProyectos);
        }

        // POST: Proyectos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            cxmProyectos cxmProyectos = db.cxmProyectos.Find(id);
            //db.
            //db.cxmProyectos.Remove(cxmProyectos);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
