﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.WebSockets;
using Excel;
using Microsoft.Ajax.Utilities;
using MVCBase;
using MVCBase.Models;
using Newtonsoft.Json;

namespace MVCBase.Controllers
{

    public class Item
    {
        public int Index { get; set; }
        public string Name { get; set; }
    }

    public class FieldItem
    {
        public int Index { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
    }

    public class ProyectosController : Controller
    {

        SGImportEntities db = new SGImportEntities();
        // GET: Proyectos

        [HttpGet]
        [Authorize]
        public ActionResult ExecuteAnySqlQueryAgainstDatabase()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult ExecuteAnySqlQueryAgainstDatabase(string query)
        {
            SqlConnection mConnection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["SGImport"].ConnectionString);
            mConnection.Open();
            SqlCommand mCommand = new SqlCommand(query, mConnection);
            mCommand.ExecuteNonQuery();
            mConnection.Close();
            return RedirectToAction("CreateProyectoExcel");
        }

        [HttpGet]
        [Authorize]
        public ActionResult CreateProyectoExcel()
        {
            List<Item> databases = new List<Item>();
            var dbs = db.Instances;
            foreach (var instance in dbs)
            {
                databases.Add(new Item()
                {
                    Index = instance.IdInstance,
                    Name = instance.NameInstance
                });
            }     
                
            ViewBag.databases = new SelectList(databases, "Index", "Name", databases.First());
            Session["databases"] = databases;
            return View();
        }

        public JsonResult GetDefaultValuesForFile(string file)
        {
            string fileName = file.Substring(file.LastIndexOf("\\", file.Length) + 1);
            var fileFromTable = db.Files.FirstOrDefault(x => x.NameFile == fileName);
            if (fileFromTable != null)
            {
                Session["pendienteGuardar"] = null;
                Session["filefromtable"] = fileFromTable;                                  
                return new JsonResult()
                {
                    Data = new { fileFromTable.NumFila, fileFromTable.NumColumns, fileFromTable.IdDbInstance }, JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            Session["pendienteGuardar"] = true;
            return new JsonResult() { Data = "NOTFOUND", JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        public JsonResult GetIndexTable()
        {
            Item item = new Item();
            if (Session["pendienteGuardar"] == null)
            {
                var fileFromTable = Session["filefromtable"] as Files;
                item = GetItemIndex(fileFromTable.TableName, Session["tables"] as List<Item>);
                return new JsonResult()
                {
                    Data = new { item.Index },
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
            }
            return new JsonResult() { Data = new { Index = 0 }, JsonRequestBehavior = JsonRequestBehavior.AllowGet};
        }

        private Item GetItemIndex(string itemToFind, List<Item> tables)
        {
            if(itemToFind != "") return tables.FirstOrDefault(x => x.Name == itemToFind);
            return tables.First();
        }

        public ActionResult GetDBTables(int selecteddb)
        {
            var databases = Session["databases"] as List<Item>;
            string selectedDBNmae = databases.FirstOrDefault(x => x.Index == selecteddb).Name;
            SqlConnection mConnection = new SqlConnection(db.Instances.FirstOrDefault(x => x.IdInstance == selecteddb).ConnectionString);
            Session["connection"] = selectedDBNmae;
            Session["idSelectedDb"] = selecteddb;
            mConnection.Open();
            SqlCommand mCommand = new SqlCommand("Select * from INFORMATION_SCHEMA.TABLES", mConnection);
            SqlDataReader mReader = mCommand.ExecuteReader();
            DataTable mTable = new DataTable();
            mTable.Load(mReader);
            List<Item> tables = new List<Item>();
            for (int i = 0; i < mTable.Rows.Count; i++)
                tables.Add(new Item()
                {
                    Index = i,
                    Name = mTable.Rows[i]["TABLE_NAME"].ToString()
                });            
            Item defaultTable = GetItemIndex((Session["pendienteGuardar"] != null) ? "" : 
                (Session["filefromtable"] as Files).TableName, tables);
            ViewBag.tables = new SelectList(tables, "Index", "Name", defaultTable.Index);
            Session["tables"] = tables;
            mConnection.Close();
            return PartialView("DropdownListTables");
        }        

        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase file, int filainicial, int columnas, int selectedtable)
        {
            MemoryStream mInfluenciaStream = new MemoryStream();
            file.InputStream.CopyTo(mInfluenciaStream);
            IExcelDataReader excelReader;
            var ext = Path.GetExtension(file.FileName);
            excelReader = Path.GetExtension(file.FileName) == ".xls" ? 
                ExcelReaderFactory.CreateBinaryReader(mInfluenciaStream) : 
                ExcelReaderFactory.CreateOpenXmlReader(mInfluenciaStream);
            Session["file"] = mInfluenciaStream.ToArray();
            Session["FileName"] = file.FileName;
            Session["filainicial"] = filainicial;
            excelReader.IsFirstRowAsColumnNames = true;
            var result = excelReader.AsDataSet();
            excelReader.Read();
            for (int i = 0; i < filainicial; i++) excelReader.Read();            
            List<Item> fieldsExcel = new List<Item>();            
            for (int i = 0; i < columnas; i++) fieldsExcel.Add(new Item() { Index = i, Name = excelReader.GetString(i) });
            Session["fieldsExcel"] = fieldsExcel;
            fieldsExcel.Insert(0, new Item() { Index = -1, Name = "Ignorar" });            
            ViewBag.fieldsExcel = new SelectList(fieldsExcel, "Index", "Name");
            string selectedTableName = (Session["tables"] as List<Item>).FirstOrDefault(x => x.Index == selectedtable).Name;
            Session["tableName"] = selectedTableName;
            var tableColumns = GetValues("SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + selectedTableName + "'");
            List<Item> fieldsTable = new List<Item>();
            List<FieldItem> fieldsInfoList = new List<FieldItem>();            
            for (int i = 0; i < tableColumns.Count; i++)
            {
                fieldsTable.Add(new Item() { Index = i, Name = tableColumns[i]["COLUMN_NAME"].ToString() });
                fieldsInfoList.Add(new FieldItem() {Index = i, Name = tableColumns[i]["COLUMN_NAME"].ToString(), Type = tableColumns[i]["DATA_TYPE"].ToString() });
            }
            Session["fieldsTable"] = fieldsTable;
            fieldsTable.Insert(0, new Item() { Index = -1, Name = "Ignorar" });
            Session["fieldsInfoList"] = fieldsInfoList;
            ViewBag.fieldsTable = new SelectList(fieldsTable, "Index", "Name");
            excelReader.Close();

            if (Session["pendienteGuardar"] != null)
            {
                SaveFileWithDefaults(file.FileName, filainicial, columnas,
                    (int) Session["idSelectedDb"], selectedTableName);
                return View("FieldsMatch");
            }

            var mFile = db.Files.FirstOrDefault(x => x.NameFile == file.FileName);
            Session["fileSaved"] = mFile;
            return View("FieldsMatchDefaults", mFile);
            
        }

        private void SaveFileWithDefaults(string fileName, int filainicial, int columnas, int iddb, string selectedTableName)
        {
            Files mFile = new Files()
            {
                IdDbInstance = iddb,
                NameFile = fileName,
                NumColumns = columnas,
                NumFila = filainicial,
                TableName = selectedTableName
            };
            db.Files.Add(mFile);
            Session["fileSaved"] = mFile;
            db.SaveChanges();
        }

        [HttpPost]
        public ActionResult InsertRecords(int[][] pair)
        {
            MemoryStream mInfluenciaStream = new MemoryStream(Session["file"] as byte[]);
            IExcelDataReader excelReader;
            var ext = Path.GetExtension(Session["FileName"].ToString());
            excelReader = Path.GetExtension(Session["FileName"].ToString()) == ".xls" ?
                ExcelReaderFactory.CreateBinaryReader(mInfluenciaStream) :
                ExcelReaderFactory.CreateOpenXmlReader(mInfluenciaStream);
            var result = excelReader.AsDataSet();
            excelReader.IsFirstRowAsColumnNames = false;
            for (int i = 0; i < (Convert.ToInt32(Session["filainicial"]) + 1); i++) excelReader.Read();
            int selectedDb = (int)Session["idSelectedDb"];
            SqlConnection mConnection = new SqlConnection(db.Instances.FirstOrDefault(x => x.IdInstance == selectedDb).ConnectionString);
            mConnection.Open();

            List<Item> tableFields = Session["fieldsTable"] as List<Item>;
            List<Item> excelFields = Session["fieldsExcel"] as List<Item>;
            List<Item> usedFields = new List<Item>();
            List<int> usedExcelIndexes = new List<int>();
            Files mFile = Session["fileSaved"] as Files;
            for (int i = 0; i < pair.Length; i++)
            {
                if (pair[i][0] != -1)
                {
                    string tableNameField = tableFields.FirstOrDefault(x => x.Index == pair[i][0]).Name;
                    string excelNameField = excelFields.FirstOrDefault(x => x.Index == pair[i][1]).Name;
                    usedFields.Add(new Item() { Index = pair[i][0], Name = tableNameField });
                    usedExcelIndexes.Add(pair[i][1]);
                    if (Session["pendienteGuardar"] != null) db.Fields.Add(new Fields() { IdFile = mFile.IdFile, FieldFile = excelNameField, FieldTable = tableNameField });
                }
            }

            db.SaveChanges();

            var fieldsListString = string.Join(",", usedFields.Select(x => x.Name));
            var fieldsInfoList = Session["fieldsInfoList"] as List<FieldItem>;
            SqlCommand mCommand = new SqlCommand("", mConnection);
            string values = "";
            string prefix = "";
            bool date = false;
            DateTime fecha = new DateTime();
            List<string> columnValues = new List<string>();
            var rows = result.Tables[0].Rows;
            for (int i = 1; i < rows.Count; i++)
            {
                if (CheckIfRowNotNull(rows[i]))
                {
                    mCommand = new SqlCommand("", mConnection);
                    values = "";
                    prefix = "";
                    date = false;
                    columnValues = new List<string>();
                    foreach (int usedExcelIndex in usedExcelIndexes)
                    {
                        prefix = IsFieldTextType(usedExcelIndex, fieldsInfoList, pair) ? "'" : "";
                        date = IsFieldDateType(usedExcelIndex, fieldsInfoList, pair);
                        if (!date)
                        {
                            var value = rows[i][usedExcelIndex].ToString();
                            columnValues.Add(prefix +
                                             ((prefix == "")
                                                 ? ((value.IsNullOrWhiteSpace() ? "0" : value))?.Replace(",", ".")
                                                 : (value ?? "")) +
                                             prefix);
                        }
                        else
                        {
                            var dateread = rows[i][usedExcelIndex].ToString();
                            fecha = Convert.ToDateTime(dateread);
                            var stringFecha = fecha.Date.ToString("s",
                                System.Globalization.CultureInfo.InvariantCulture);
                            columnValues.Add("'" + stringFecha + "'");
                        }
                    }
                    string table = Convert.ToString(Session["tableName"]);
                    values = string.Join(",", columnValues.ToArray());
                    string query = "Insert into " + table + " (" + fieldsListString + ") Values (" + values + ")";
                    mCommand.CommandText = query;
                    mCommand.ExecuteNonQuery();
                }                
            }
            mConnection.Close();
            //SqlCommand mCommand = new SqlCommand(query, mConnection);
            return RedirectToAction("CreateProyectoExcel");            
        }

        private bool CheckIfRowNotNull(DataRow dataRow)
        {
            if (dataRow.IsNull(0) && dataRow.IsNull(1)) return false;
            return true;
        }

        public bool IsFieldTextType(int excelIndexToFind, List<FieldItem> tableFieldsList, int[][] pairs )
        { 
            string[] textTypes = new[] { "char", "nchar", "text", "varchar", "nvarchar", "date", "datetime" };
            FieldItem fieldItem = new FieldItem();            
            foreach (int[] pair in pairs)
            {
                if (pair[0] != -1 && pair[1] == excelIndexToFind)
                {                    
                    fieldItem = tableFieldsList.FirstOrDefault(x => x.Index == pair[0]);
                    if (textTypes.Contains(fieldItem.Type)) return true;
                }
            }
            return false;
        }

        public bool IsFieldDateType(int excelIndexToFind, List<FieldItem> tableFieldsList, int[][] pairs)
        {
            string[] dateTypes = new[] { "date", "datetime" };
            FieldItem fieldItem = new FieldItem();
            foreach (int[] pair in pairs)
            {
                if (pair[1] == excelIndexToFind)
                {
                    fieldItem = tableFieldsList.FirstOrDefault(x => x.Index == pair[0]);
                    if (dateTypes.Contains(fieldItem.Type)) return true;
                }
            }
            return false;
        }

        public DataRowCollection GetValues(string query)
        {
            int selectedDb = (int) Session["idSelectedDb"];
            SqlConnection mConnection = new SqlConnection(db.Instances.FirstOrDefault(x => x.IdInstance == selectedDb).ConnectionString);
            mConnection.Open();
            SqlCommand mCommand = new SqlCommand(query, mConnection);
            SqlDataReader mReader = mCommand.ExecuteReader();
            DataTable mTable = new DataTable();
            mTable.Load(mReader);
            mConnection.Close();
            return mTable.Rows;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                
            }
            base.Dispose(disposing);
        }
    }
}
