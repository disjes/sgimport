﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCBase.Models
{
    public class CatastroCuentasViewModel
    {
        public string CatCca { get; set; }
        public DatosColonia DatosColonia { get; set; }
        public decimal MontoRecuperar { get; set; }
        public decimal MontoPagado { get; set; }
        public decimal MontoPendiente { get; set; }
    }

    public class DatosColonia
    {
        public string NombreColonia { get; set; }
        public string SectorColonia { get; set; }
        public string DistritoColonia { get; set; }
        public string ProyectoColonia { get; set; }
    }
}