﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCBase.Models
{
    public class CuentasPorFranjaViewModel
    {
        public int IdInmueblePorProyecto { get; set; }
        public int? NumFranja { get; set; }
        public string CatCca { get; set; }
        public decimal? AreaTerreno { get; set; }        
        public decimal? FactorDistancia { get; set; }
        public decimal? FactorArea { get; set; }
        public decimal? FactorUso { get; set; }
        public decimal? SocioEconomico { get; set; }
        public decimal? AccesoAjuste { get; set; }
        public decimal? FactorIndividual { get; set; }
        public decimal AreaGravable { get; set; }
        public double FactorConversion { get; set; }
        public double Gravamen { get; set; }

        public string FactorIndividualStr { get; set; }
        public string AreaGravableStr { get; set; }
        public string FactorConversionStr { get; set; }
        public string GravamenStr { get; set; }

    }

    public class FranjaViewModel
    {
        public int IdFranja { get; set; }
        public int IdProyecto { get; set; }
        public int NumFranja { get; set; }
        public int Porcentaje { get; set; }
        public bool EsDirecto { get; set; }

        public ICollection<CuentasPorFranjaViewModel> cxmInmueblesPorProyecto { get; set; }

    }

    public class InfluenciaViewModel
    {
        public int NumInfluencia { get; set; }
        public string Porcentaje { get; set; }
        public string TotalAreasGravables { get; set; }
        public string FactorConversion { get; set; }
        public string Gravamenes { get; set; }
    }

}